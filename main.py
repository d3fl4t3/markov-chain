#!/usr/bin/python3
import string

import click
import os
import requests
import rethinkdb as r

from config import BOT_TOKEN, CHAT_ID, DB

_db = r.db("markov")


def strip_word(word):
    allowed = "абвгдежзийклмнопрстуфхцчшщьыъэюя" + string.ascii_lowercase + string.digits + "-+"
    return ''.join(list(filter(lambda x: x in allowed, word.lower().replace("ё", "е")))[:40])


def load_messages(messages):
    c = r.connect(*DB)
    for message in messages:
        words = ["<START>", *[strip_word(w) for w in message.split(" ") if strip_word(w)], "<END>"]
        for prev_word, next_word in zip(words, words[1:]):
            res = _db.table('chain').insert({
                "word": prev_word,
                "next": {next_word: 1}
            }, conflict=lambda word, old_doc, new_doc: old_doc.merge({
                "next": {
                    next_word: old_doc["next"][next_word].default(0) + 1
                }
            })).run(c)
            if res["errors"]:
                raise Exception(res)
        yield message


def make_message(start_word="<START>", min_len=None, max_len=None):
    c = r.connect(*DB)
    get_next_words = lambda word: _db.table("chain").get(word)["next"].coerce_to("array")
    choose_random = lambda word: get_next_words(word).concat_map(lambda w: r.expr([w[0]]) * w[1]).sample(1)[0]
    words = [start_word]
    while True:
        words += [choose_random(words[-1]).run(c)]
        if words[-1] == "<END>":
            break
    if start_word == "<START>":
        del words[0]  # start
    del words[-1]  # end
    if min_len and len(words) < min_len or max_len and len(words) > max_len:
        return make_message(start_word, min_len, max_len)
    return ' '.join(words)


@click.group()
def cli():
    pass


@cli.command()
@click.argument("file")
def load(file):
    total = os.stat(file).st_size
    with click.progressbar(length=total, label="Loading messages", show_pos=True) as bar:
        with open(file) as f:
            for message in load_messages(f):
                bar.update(len(message.encode()))


@cli.command()
@click.option("--count", "-c", default=1, type=click.INT)
@click.option("--start-word", "-s")
@click.option("--min-len", "-m", help="Minimal length of message (in words)", default=3, type=click.INT)
@click.option("--max-len", "-M", help="Maximum length of message (in words)", type=click.INT)
def generate(count, start_word=None, min_len=None, max_len=None):
    for i in range(count):
        kwargs = {}
        if start_word:
            kwargs["start_word"] = start_word
        if min_len:
            kwargs["min_len"] = min_len
        if max_len:
            kwargs["max_len"] = max_len
        click.echo(make_message(**kwargs))


if __name__ == "__main__":
    cli()
